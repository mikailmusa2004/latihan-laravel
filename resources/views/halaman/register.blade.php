<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>form</title>
</head>
    <body>
         <div>
            <h1>Buat Account Baru!</h1>
            <h3><b>Sign Up Form</b></h3>
        </div>

        <!-- Form biodata calon user-->
        
        <form action="/register" method="post">
            @csrf
            <div>
                
                <p>First name:</p>
                    <label for="first_name">
                    </label>
                    <input type="text" name="namadepan">
                
                <p>Last name:</p>
                    <input type="text" name="namabelakang">
                
            </div>

            <div>
                <p>Gender:</p>
                
                    <input type="radio" name="gndr" value="lk">Male <br>
                    <input type="radio"  name="gndr" value="pr">Female <br>
                    <input type="radio"  name="gndr" value="lain">other
            </div>

            <div>
                <p>Nationality:</p>
                <select>
                    <option value="ind">Indonesia</option>
                    <option value="amk">Amerika</option>
                    <option value="ing">Inggris</option>
                </select>
            </div>

            <div>
                <p>Language Spoken:</p>
                    <input type="checkbox" name="ls" value="ind">Bahasa Indonesia <br>
                    <input type="checkbox" name="ls" value="amk">English <br>
                    <input type="checkbox" name="ls" value="lain">Other <br>
            </div> 
            
            <div>
                <p>Bio:</p>
                <textarea cols="30" rows="7"></textarea> <br>
                
                
            </div> 
        <input type="submit" value="Sign Up">
        </form>       
    </body> 
</html>