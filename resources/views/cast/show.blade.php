@extends('layout.master')

@section('judul')
    Detail pemain {{$cast->nama}}
@endsection

@section('content')
    
<h3>{{$cast->nama}}</h3>
<p>Umur : {{$cast->umur}}</p>
<p>Biodata : {{$cast->bio}}</p>
@endsection